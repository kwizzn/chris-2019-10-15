### BASE
FROM node:12-alpine AS base
WORKDIR /app
RUN apk add --no-cache curl && chown node:node /app && npm install -g serve
USER node
HEALTHCHECK --interval=30s --timeout=3s CMD curl -f http://localhost:3000/ || exit 1

### DEV
FROM base AS dev
COPY --chown=node:node . .
RUN npm install
RUN npm run build
CMD ["npm", "start"]

### PROD
FROM base AS prod
COPY --from=dev --chown=node:node /app/build /app
CMD ["serve", "-s", "."]
