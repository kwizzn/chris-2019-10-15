Chris - Oct 15 2019
===================

## Installation

    $ docker-compose up                    # Run in development mode
    $ docker-compose -f docker-compose.yml # Run in production mode

Be aware that you need to **re-build the Docker container target** before switching environments:
   
    $ docker-compose build                       # Build for development mode
    $ docker-compose -f docker-compose.yml build # Build for production mode


## Tests

     $ docker-compose run app npm run test


## Checklist

General
  - [x] Can run app within Docker without installing Node.js on host

Docker
  - [x] App runs within a Docker container and app is accessible in host’s web browser
  - [x] Rebuilding the Docker image will install any changes to Node modules
  - [x] Changing a file will automatically rebuild app within Docker

Continuous Integration
  - [x] Use Docker images for runners. See ​document for using Docker images in GitLab CI​.
  - [x] Build the app
  - [x] Run the tests and generate code coverage repo. See ​Coverage Report​ below.

Review Apps
  - [x] Demo React App
  - [x] Code Coverage Report
  - [x] Review Apps are separated per branch
  - [x] Use AWS S3 (free tier) for hosting the static files
  - [x] Demonstrate it works by creating a small Merge Requests with a change

Custom
  - [x] State is persisted in local storage
  - [x] Progressive web app (PWA)
  - [x] Docker healthcheck using cURL
  - [x] ESLint configured for `react-app` coding style
  - [x] husky package watches the pre-commit & pre-push hooks
  - [x] `npm audit` monitors security issues as a step in the build
  - [x] Tests need to pass before anything is deployed
  - [x] Production environment is built alongside master builds, the deployment must be triggered manually


## Demo

**Merge Request:** https://gitlab.com/kwizzn/chris-2019-10-15/merge_requests/2

**App Review App Feature:** http://todomvc.type10.s3-website-eu-west-1.amazonaws.com/features/demo-feature/

**Coverage Review App:** http://todomvc.type10.s3-website-eu-west-1.amazonaws.com/features/demo-feature/coverage/


## Features
  - State is persisted in local storage and available after a page reload. An error is shown in case the `state` property contains invalid JSON. Feel freee to test this by messing up the `state` property in dev tools.
  - A very basic progressive web app (PWA) manifest with icons allows adding the app to the home screen.
  - The `Dockerfile` contains a multi-stage build. `docker-compose -f docker-compose.yml build` will build a clean container that only contains the built app which is served by the `serve` static server.
  - `docker-compose-override.yml` mounts the working directory into the container's `/app` directory, except for the `node_modules/` directory. This may seem inconvenient because it requires a build of the container each time packages are modified. This way however, errors that result from native addon builds (e.g. when developing on Mac or Windows machines) can be detected early on during development because the container's `node_modules/` are used.
  - `npm audit` will fail the build in case of critical security issues.
  - The AWS side of things was created using a CloudFormation template that can be reviewed in `/cloudformation.json`.
  - [Stage environment](http://todomvc.type10.s3-website-eu-west-1.amazonaws.com/stage/) gets deployed when master branch is pushed.
  - [Production environment](http://todomvc.type10.s3-website-eu-west-1.amazonaws.com/) must be deployed manually in GitLab CI.
  - Review Apps deployed to feature branches contain the GitLab CI Visual Review feedback form.


## Security
  - [x] Credentials are stored in GitLab secure environment variables (clean VCS)
  - [x] npm package vulnerabilities are monitored in the build using `npm audit`
  - [x] No direct push to the master branch is permitted, every change needs to be merged from an approved merge request
  - [x] Nothing gets deployed to production that was not tested and audited  
  - [ ] Currently, the apps are served by an S3 bucket that is directly available to the public and without HTTPS (see improvements)
  - [ ] Currently, the apps are prone to session hijacking, click jacking, XSS, MIME type sniffing (see improvements)
  

## Improvements
  - AWS CloudFront should be wrapped around the S3 bucket(s) (solves the HTTPS issue, allows for caching and supports custom HTTP headers)
  - AWS Lambda Edge should be configured on the CloudFormation origin or viewer response level to add HTTP security headers like `Strict-Transport-Security`, `content-security-policy`, `x-content-type-options`, `x-frame-options` & `x-xss-protection`
  - The production environment should make sure that source maps are not available to the public
  - The stage environment should not be available to the public
  - The production & stage environments should live on their own S3 buckets
  - In order to improve loading times, larger projects should be split into multiple smaller bundles that are loaded in parallel or on demand, `webpack` allows for that
  - Failing builds should send notifications to team messaging apps like Slack or Discord
  - A "flow"-like Git branching model would allow for easier hot fixing but that is a matter of taste
  - A prototyping tool like React-Proto, Storybook or Styleguidist could come in handy for showcasing and testing individual React components.
  - ESLint extends the `react-app` coding style because the todomvc codebase was built upon it. It may be a good decision to enforce a more restrictive coding style.
