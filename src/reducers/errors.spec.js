import errors from './errors'
import * as types from '../constants/ActionTypes'

describe('errors reducer', () => {
  it('should handle initial state', () => {
    expect(
      errors(undefined, {})
    ).toEqual([])

    expect(
      errors([{ id: 2, message: 'Ooops' }], {})
    ).toEqual([{ id: 2, message: 'Ooops' }])
  })

  it('should handle REMOVE_ERROR', () => {
    expect(
      errors([
        {
          message: 'That is an error',
          id: 1
        },
        {
          message: 'Whoops',
          id: 2
        }
      ], {
        type: types.REMOVE_ERROR,
        id: 1
      })
    ).toEqual([
      {
        message: 'Whoops',
        id: 2
      }
    ])
  })
})
