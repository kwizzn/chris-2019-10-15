import { combineReducers } from 'redux'
import todos from './todos'
import visibilityFilter from './visibilityFilter'
import errors from './errors'

const rootReducer = combineReducers({
  todos,
  visibilityFilter,
  errors
})

export default rootReducer
