import { ADD_ERROR, REMOVE_ERROR } from '../constants/ActionTypes'

const errors = (state = [], action) => {
  switch (action.type) {

    case ADD_ERROR:
      return state.concat([action.error])

    case REMOVE_ERROR:
      return state.filter(error => error.id !== action.id)

    default:
      return state
  }
}

export default errors
