import React from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './components/App'
import Errors from './containers/Errors'
import reducer from './reducers'
import 'todomvc-app-css/index.css'

let state = {};
if (localStorage.getItem('state')) {
  try {
    state = JSON.parse(localStorage.getItem('state'))
  } catch (e) {
    state = { errors: [{ id: 1, message: 'Error restoring saved state!' }]}
  }
}

const store = createStore(reducer, state)

store.subscribe(() => {
  localStorage.setItem('state', JSON.stringify(store.getState()))
})

render(
  <Provider store={store}>
    <App />
    <Errors />
  </Provider>,
  document.getElementById('root')
)
