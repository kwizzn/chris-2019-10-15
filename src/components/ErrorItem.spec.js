import React from 'react'
import { createRenderer } from 'react-test-renderer/shallow';
import ErrorItem from './ErrorItem'

const setup = () => {
  const props = {
    error: {
      id: 1,
      message: 'That is an error'
    },
    removeError: jest.fn(),
  }

  const renderer = createRenderer()

  renderer.render(
    <ErrorItem {...props} />
  )

  let output = renderer.getRenderOutput()

  return {
    props: props,
    output: output,
  }
}

describe('components', () => {
  describe('ErrorItem', () => {
    it('initial render', () => {
      const { output } = setup()
      expect(output.type).toBe('li')
      expect(typeof output.props.style).toBe('object')

      const span = output.props.children[0]
      expect(span.type).toBe('span')
      expect(typeof span.props.style).toBe('object')

      expect(output.props.children[1]).toEqual('That is an error')
    })

    it('onClick should call removeError', () => {
      const { output, props } = setup()
      output.props.onClick({})
      expect(props.removeError).toBeCalledWith(1)
    })
  })
})
