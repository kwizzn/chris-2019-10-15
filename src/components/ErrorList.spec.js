import React from 'react'
import { createRenderer } from 'react-test-renderer/shallow';
import ErrorList from './ErrorList'
import ErrorItem from './ErrorItem'

const setup = () => {
  const props = {
    errors: [
      {
        id: 1,
        message: 'Ooops'
      }, {
        id: 2,
        message: 'That is an error'
      }
    ],
    actions: {
      removeError: jest.fn(),
    }
  }

  const renderer = createRenderer();
  renderer.render(<ErrorList {...props} />)
  const output = renderer.getRenderOutput()

  return {
    props: props,
    output: output
  }
}

describe('components', () => {
  describe('ErrorList', () => {
    it('should render container', () => {
      const { output } = setup()
      expect(output.type).toBe('ul')
      expect(typeof output.props.style).toBe('object')
    })

    it('should render errors', () => {
      const { output, props } = setup()
      expect(output.props.children.length).toBe(2)
      output.props.children.forEach((error, i) => {
        expect(error.type).toBe(ErrorItem)
        expect(Number(error.key)).toBe(props.errors[i].id)
        expect(error.props.error).toBe(props.errors[i])
      })
    })
  })
})
