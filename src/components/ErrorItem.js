import React, { Component } from 'react'
import PropTypes from 'prop-types'

const liStyle = {
  backgroundColor: '#ffbaba',
  color: '#d8000c',
  fontSize: '14px',
  fontWeight: 'bold',
  left: 0,
  listStyle: 'none',
  margin: 0,
  padding: '10px 10px 10px 35px',
  position: 'relative'
}

const spanStyle = {
  backgroundColor: '#d8000c',
  borderRadius: '50%',
  color: '#ffbaba',
  cursor: 'pointer',
  display: 'block',
  fontSize: '20px',
  height: '20px',
  left: '10px',
  lineHeight: '14px',
  marginTop: '-10px',
  position: 'absolute',
  textAlign: 'center',
  top: '50%',
  width: '20px'
}

export default class ErrorItem extends Component {
  static propTypes = {
    error: PropTypes.object.isRequired,
    removeError: PropTypes.func.isRequired,
  }

  render() {
    const { error, removeError } = this.props

    return (
      <li key={error.id} onClick={() => removeError(error.id)} style={liStyle}><span style={spanStyle}>×</span>{error.message}</li>
    )
  }
}
