import React from 'react'
import PropTypes from 'prop-types'
import ErrorItem from './ErrorItem'

const style = {
  background: 'rgba(0, 0, 0, 0.5)',
  bottom: 0,
  height: '1000%',
  left: 0,
  padding: '10px',
  position: 'absolute',
  right: 0,
  top: '-144px',
  zIndex: 2,
}

const ErrorList = ({ errors, actions }) =>
  (
    errors.length ?
      <ul style={style}>
        {errors.map(error =>
          <ErrorItem key={error.id} error={error} {...actions} />
        )}
      </ul>
      : <ul />
  )

ErrorList.propTypes = {
  errors: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    message: PropTypes.string.isRequired
  }).isRequired).isRequired,
  actions: PropTypes.object.isRequired
}

export default ErrorList;
